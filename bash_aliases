alias sort="LC_ALL=C sort" # Makes GNU sort _much_ faster
alias grep="grep --color"  # Enable grep colours
alias g="git"
alias ls="ls --color=auto"
# alias redshift="redshift -l 37.7490:144.9120"
alias pip="python -m pip"
# alias ls="ls --color=auto"
alias j="cat ${1} | jq ."

